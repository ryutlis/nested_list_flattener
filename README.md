## Nested Integer List Flattener

#### Overview
This is a tool for flattening nested lists contains only integers.

In order to support nested lists that have very deep nested levels, it can also take input formats like stringified list or stringified list stored in a file.

At the moment, 3 input formats are supported: plain Python list object, list stringified as a Python string and list stringified as a file (can be also viewed as a JSON file contains a list)

#### Usage

``` python
# flatten a Python list

>>> from flattener import NestedIntegerListFlattener
>>> nested_list = [1, [2, 3], 4]
>>> list_flattener = NestedIntegerListFlattener(NestedIntegerListFlattener.INPUT_TYPE_LIST)
>>> list_flattener.flatten(nested_list)
[1, 2, 3, 4]

# flatten a stringified Python list

>>> from flattener import NestedIntegerListFlattener
>>> nested_list_stringified = '[1, [2, 3], 4]'
>>> list_flattener = NestedIntegerListFlattener(NestedIntegerListFlattener.INPUT_TYPE_STRING)
>>> list_flattener.flatten(nested_list_stringified)
'[1,2,3,4]'


# flatten a file contains a stringified Python list

>>> from flattener import NestedIntegerListFlattener
>>> input_filename = 'nested_list.txt'
>>> output_filename = 'flattened_list.txt'
>>> list_flattener = NestedIntegerListFlattener(NestedIntegerListFlattener.INPUT_TYPE_FILENAME)
>>> list_flattener.flatten(input_filename, output_filename)
True
```

#### Running tests

##### Test functionality

``` bash
 * py.test -v tests/test_functionality.py
=============================================== test session starts ================================================
platform darwin -- Python 2.7.10, pytest-2.8.1, py-1.4.30, pluggy-0.3.1 -- /usr/local/opt/python/bin/python2.7
cachedir: tests/.cache
rootdir: /Users/ryutlis/sandbox/nested_list_flattener/tests, inifile:
collected 11 items

tests/test_functionality.py::test_empty_list_success PASSED
tests/test_functionality.py::test_empty_list_string_success PASSED
tests/test_functionality.py::test_already_flattened_list_success PASSED
tests/test_functionality.py::test_already_flattened_list_string_success PASSED
tests/test_functionality.py::test_normal_nested_list_success PASSED
tests/test_functionality.py::test_normal_nested_list_string_success PASSED
tests/test_functionality.py::test_wrong_input_type_failure PASSED
tests/test_functionality.py::test_list_illegal_character_validation_failure PASSED
tests/test_functionality.py::test_list_string_illegal_character_validation_failure PASSED
tests/test_functionality.py::test_list_string_brackets_mismatch_validation_failure PASSED
tests/test_functionality.py::test_deep_nested_list_success PASSED

============================================ 11 passed in 0.01 seconds ============================================
```

##### Test scalability

``` bash
 * python2.7 tests/test_scalability.py
Creating file that contains nested list with 100000000 levels
Time used: 77.764580965 seconds
Flattening ...
Time used: 110.344867945 seconds
```
