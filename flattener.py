import ast

SIZE_10_MB = 1024 * 1024 * 10


class StringifyListValidationError(Exception):
    pass


class NestedIntegerListFlattener(object):
    """
    Tool for flattening arbitrary nested lists of integers

    Currently support 3 input formats:
    1. List
    2. Stringified List
    3. Stringified List stored in a file

    Note:
    The reason for supporting formats other than native Python list is due to
    its recursion level limitation. CPython internally uses recursive approach
    to, for example, construct the __repr__ of a list, when such list reaches
    the limit nested level, CPython cannot properly process this list.
    """

    INPUT_TYPE_LIST = 1
    INPUT_TYPE_STRING = 2
    INPUT_TYPE_FILENAME = 3

    def __init__(self, input_type,
                 file_read_buf_size=SIZE_10_MB,
                 file_write_buf_size=SIZE_10_MB):
        self._input_type = input_type
        self._file_read_buf_size = file_read_buf_size
        self._file_write_buf_size = file_write_buf_size

    def flatten(self, input_source, output_destination=None):
        if self._input_type == self.INPUT_TYPE_LIST:
            return self._flatten_list(input_source)
        elif self._input_type == self.INPUT_TYPE_STRING:
            return self._flatten_string(input_source)
        elif self._input_type == self.INPUT_TYPE_FILENAME:
            return self._flatten_file(input_source, output_destination)

    def _flatten_list(self, input_list):
        if not isinstance(input_list, list):
            raise TypeError("Expect input to be of type list, not {}"
                            .format(type(input_list)))
        output = self._flatten_stringified_list(str(input_list))
        return ast.literal_eval(output)

    def _flatten_string(self, input_string):
        if not isinstance(input_string, str):
            raise TypeError("Expect input to be of type str, not {}"
                            .format(type(input_string)))
        return self._flatten_stringified_list(input_string)

    def _flatten_file(self, input_filename, output_filename):
        if output_filename is None:
            raise ValueError("output filename should be specified")

        with open(input_filename, 'r') as input_f:
            with open(
                output_filename,
                'w',
                self._file_write_buf_size
            ) as output_f:

                output_f.write('[')

                try:
                    for char in self._do_flatten_generator(input_f):
                        output_f.write(char)
                except StringifyListValidationError:
                    output_f.truncate(0)
                    return False
                except:
                    raise

                output_f.write(']')
                return True

    def _flatten_stringified_list(self, stringified_list):
        return '[{}]'.format(
            ''.join(self._do_flatten_generator(stringified_list))
        )

    def _do_flatten_generator(self, stringified_list):
        numbers = set(map(str, xrange(10)))
        legal_chars = set([' ', '\n'])
        left_bracket_count = 0
        right_bracket_count = 0

        if isinstance(stringified_list, file):
            stringified_list = self._file_to_char_generator(stringified_list)

        for current_char in stringified_list:
            if current_char in numbers:
                yield current_char
            elif current_char == ',':
                yield ','
            elif current_char == '[':
                left_bracket_count += 1
            elif current_char == ']':
                right_bracket_count += 1
            elif current_char in legal_chars:
                continue
            else:
                raise StringifyListValidationError(
                    "Illegal character encountered in the list"
                )

        if (left_bracket_count != right_bracket_count):
            raise StringifyListValidationError(
                "Square brackets in input are not balanced"
            )

    def _file_to_char_generator(self, file_obj):
        while True:
            chars = file_obj.read(self._file_read_buf_size)
            if chars == '':
                break
            for char in chars:
                yield char
