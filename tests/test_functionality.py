import pytest
from flattener import (
    NestedIntegerListFlattener, StringifyListValidationError
)


def test_empty_list_success():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )
    assert list_flattener.flatten([]) == []


def test_empty_list_string_success():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )
    assert list_flattener.flatten('[]') == '[]'


def test_already_flattened_list_success():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )
    assert list_flattener.flatten([1, 2, 3]) == [1, 2, 3]


def test_already_flattened_list_string_success():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )
    assert list_flattener.flatten('[1,2,3,4]') == '[1,2,3,4]'


def test_normal_nested_list_success():
    nested_list = [1, [2, 3], [4, 5, [6]], [7], [8, 9, 10], 11, 12, [13]]
    expected = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )

    assert list_flattener.flatten(nested_list) == expected


def test_normal_nested_list_string_success():
    nested_list = '[1, [2, 3], [4, 5, [6]], [7], [8, 9, 10], 11, 12, [13]]'
    expected = '[1,2,3,4,5,6,7,8,9,10,11,12,13]'

    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )

    assert list_flattener.flatten(nested_list) == expected


def test_wrong_input_type_failure():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )

    with pytest.raises(TypeError):
        list_flattener.flatten('[]')

    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )

    with pytest.raises(TypeError):
        list_flattener.flatten([])


def test_list_illegal_character_validation_failure():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )

    with pytest.raises(StringifyListValidationError):
        list_flattener.flatten([[1], 'a'])


def test_list_string_illegal_character_validation_failure():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )

    with pytest.raises(StringifyListValidationError):
        list_flattener.flatten('[[1], "a"]')


def test_list_string_brackets_mismatch_validation_failure():
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_STRING
    )

    with pytest.raises(StringifyListValidationError):
        list_flattener.flatten('[1, [2, 3]')


def test_deep_nested_list_success():
    def generate_nested_list(level):
        original = l = [1, ]

        for i in xrange(level - 1):
            l.append([1, ])
            l = l[1]
        return original

    level = 100
    nested_list = generate_nested_list(level)
    expected_flattened_list = [1] * level

    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_LIST
    )

    assert list_flattener.flatten(nested_list) == expected_flattened_list
