"""
This test ensures that when nested level gets really big,
NestedIntegerListFlattener is still able to flatten it
"""

import timeit
from flattener import NestedIntegerListFlattener

# 100 million levels deep
NEST_LEVEL = 100000000

INPUT_FILENAME = '/tmp/nested_list.txt'
OUTPUT_FILENAME = '/tmp/flattened_list.txt'


def create_nested_list_file(n_level=NEST_LEVEL, filename=INPUT_FILENAME):
    with open(filename, 'wb', 1024 * 1024 * 10) as f:
        for _ in xrange(n_level):
            f.write('[1,')
        for _ in xrange(n_level):
            f.write(']')


def flatten_list(input_filename=INPUT_FILENAME,
                 output_filename=OUTPUT_FILENAME):
    list_flattener = NestedIntegerListFlattener(
        NestedIntegerListFlattener.INPUT_TYPE_FILENAME
    )
    result = list_flattener.flatten(input_filename, output_filename)
    assert result is True


def main():
    print("Creating file that contains nested list with {} levels"
          .format(NEST_LEVEL))
    time_input_creation = timeit.timeit(
        'create_nested_list_file()',
         setup='from __main__ import create_nested_list_file',
         number=1
    )
    print("Time used: {} seconds".format(time_input_creation))

    print("Flattening ...")
    time_flatten = timeit.timeit(
        'flatten_list()',
        setup='from __main__ import flatten_list',
        number=1
    )
    print("Time used: {} seconds".format(time_flatten))


if __name__ == "__main__":
    main()
